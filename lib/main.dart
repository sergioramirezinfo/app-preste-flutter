import 'package:app_preste/pages/ocrTest.dart';
import 'package:app_preste/pages/pag1.dart';
import 'package:app_preste/pages/pag2.dart';
import 'package:app_preste/pages/pag3.dart';
import 'package:app_preste/pages/pag4.dart';
import 'package:app_preste/pages/pag5.dart';
import 'package:app_preste/pages/pag6.dart';
import 'package:app_preste/pages/pagOCR.dart';
import 'package:app_preste/pages/pagTerminos.dart';
import 'package:app_preste/presentation/screens/bienvenido_screen.dart';
import 'package:app_preste/presentation/screens/pagina10_screen.dart';
import 'package:app_preste/presentation/screens/pagina11_screen.dart';
import 'package:app_preste/presentation/screens/pagina12_screen.dart';
import 'package:app_preste/presentation/screens/pagina8_screen.dart';
import 'package:app_preste/presentation/screens/pagina9_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky,
        overlays: [SystemUiOverlay.bottom]);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: GoogleFonts.patrickHandTextTheme()
      ),
      debugShowCheckedModeBanner: false,
      // home: const MyHomePage(title: 'Flutter Demo Home Page'),
      initialRoute: '/sergio',
      routes: {
        '/sergio'  : (context) => const MyHomePage(title: 'Flutter Demo Home Page'),
        '/pagina7'  : (context) => const BienvenidoScreen(),
        '/pagina8'  : (context) => const Pagina8Screen(),
        '/pagina9'  : (context) => const Pagina9Screen(),
        '/pagina10' : (context) => const Pagina10Screen(),
        '/pagina11' : (context) => const Pagina11Screen(),
        '/pagina12' : (context) => const Pagina12Screen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 1, "Pag 1"),
                iconX(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 2, "Pag 2"),
                iconX(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 3, "Pag 3"),
                iconX(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 4, "Pag 4"),
                iconCheck(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 0, "Terminos"),
                iconCheck(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 98, "Pag OCR"),
                iconCheck(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 99, "OCR"),
                iconCheck(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 5, "Pag5"),
                iconCheck(context)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                containerBoton(context, 0.1, 6, "pag6"),
                iconCheck(context)
              ],
            ),
          ]),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget containerBoton(
      BuildContext context, double porcentajeAlto, int nroPag, String nombre) {
    return Container(
      // width: MediaQuery.of(context).size.width * 0.2,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
            child: ElevatedButton.icon(
              onPressed: () => {
                if (nroPag == 0)
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => pagTerminos())),
                if (nroPag == 1)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag1())),
                if (nroPag == 2)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag2())),
                if (nroPag == 3)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag3())),
                if (nroPag == 4)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag4())),
                if (nroPag == 5)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag5())),
                if (nroPag == 6)
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pag6())),
                if (nroPag == 98)
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => pagOCR())),
                if (nroPag == 99)
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomeScreen())),
              },
              icon: const Icon(
                Icons.arrow_forward,
                size: 15,
              ),
              label: Text(nombre),
              style: ElevatedButton.styleFrom(
                foregroundColor: Color(0xFFF4601F),
                backgroundColor: Colors.white,
                minimumSize: Size(150, 50),
                textStyle: const TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFF4601F),
                  fontSize: 18,
                ),
                side: const BorderSide(
                  color: Color(0xFFF4601F),
                  width: 3,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35),
                ),
                elevation: 2,
                padding: EdgeInsets.zero,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget iconCheck(BuildContext context) {
    return Icon(
      Icons.check,
      color: Colors.green,
    );
  }

  Widget iconX(BuildContext context) {
    return Icon(
      Icons.close,
      color: Colors.red,
    );
  }

  Widget texto1(String x) {
    return Text(
      x,
      style: const TextStyle(
        fontFamily: 'Poppins',
        fontSize: 18,
        letterSpacing: 1,
        fontWeight: FontWeight.w600,
      ),
    );
  }
}
