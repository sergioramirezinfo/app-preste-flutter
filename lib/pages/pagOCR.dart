import 'package:app_preste/pages/pag2.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_tesseract_ocr/flutter_tesseract_ocr.dart';
import 'package:flutter/cupertino.dart';

class pagOCR extends StatefulWidget {
  pagOCR({Key? key}) : super(key: key);

  @override
  State<pagOCR> createState() => _pagOCRState();
}

class _pagOCRState extends State<pagOCR> {
  // imagenes
  List<String> vectorImagenes = [
    'default',
    'assets/images/casa.png',
    'assets/images/auto.png',
    'assets/images/avion.png',
    'assets/images/negocios.png',
    'assets/images/salud.png',
    'assets/images/Otro.png'
  ];
  List<String> nombreImagen = [
    'default',
    'casa',
    'auto',
    'avion',
    'negocios',
    'salud',
    'Otro'
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Color.fromARGB(0, 0, 0, 0),
        body: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 1,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 255, 255, 255),
                Color(0xFFFDEAB1),
                Color(0xFFFD8641),
              ],
              stops: [0, 0.6, 1],
              begin: AlignmentDirectional(0, 1),
              end: AlignmentDirectional(0, -1),
            ),
          ),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(0, 1),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(10, 100, 10, 0),
                      child: Material(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Container(
                            width: double.infinity,
                            height: MediaQuery.of(context).size.height * 0.65,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 255, 255, 255),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 4,
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  offset: Offset(1, -1),
                                )
                              ],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: // Generated code for this ColumnContainer Widget...
                                Padding(
                              padding:
                                  EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: texto1Negrilla(
                                              'Toma una fotografia a tu carnet de identidad:',
                                              22),
                                        ),
                                        texto1Puntaje('(+100 pts)', 14),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      columFotoOCR("Anverso", 1, context),
                                      columFotoOCR("Reverso", 2, context),
                                    ],
                                  ),
                                  Expanded(
                                    child: texto1Normal(
                                        'Anverso',18),
                                  ),
                                  texto1Normal(
                                      _extractText1,18),
                                  // Expanded(
                                  //   child: texto1Normal(
                                  //       'Reverso',18),
                                  // ),
                                  // Expanded(
                                  //   child: texto1Normal(
                                  //       _extractText2,18),
                                  // ),
                                ],
                                
                              ),
                            )),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    containerBoton(context, 0.1)
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 124,
                decoration: BoxDecoration(
                  color: Color(0x00FFFFFF),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  color: Color(0x00FD8641),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.height * 0.15,
                      decoration: BoxDecoration(
                        color: Color(0x00FFFFFF),
                      ),
                      child: Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.asset(
                            'assets/images/logo_1_1.png',
                            width: 300,
                            height: 200,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }

  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }

  Widget texto1Normal(String x, double tam) {
    return Text(
      x,
      textAlign: TextAlign.justify,
      style: TextStyle(
        fontFamily: 'Poppins',
        fontSize: tam,
        letterSpacing: 1,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
            child: ElevatedButton.icon(
              onPressed: () async {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => pag2()));
              },
              icon: const Icon(
                Icons.arrow_forward,
                size: 15,
              ),
              label: const Text('Volver'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Color(0xFFF4601F),
                backgroundColor: Colors.white,
                minimumSize: Size(150, 50),
                textStyle: const TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFF4601F),
                  fontSize: 18,
                ),
                side: const BorderSide(
                  color: Color(0xFFF4601F),
                  width: 3,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35),
                ),
                elevation: 2,
                padding: EdgeInsets.zero,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget texto1Negrilla(String x, double tam) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: 'Poppins',
        fontSize: tam,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget texto1Puntaje(String x, double tam) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: x,
        color: Color(0xFFE54B0A),
        fontSize: tam,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  bool _scanning1 = false;
  bool _scanning2 = false;
  String _extractText1 = '';
  String _extractText2 = '';
  late File? _pickedImage1 = null;
  late File? _pickedImage2 = null;

  Widget columFotoOCR(String nom, int nroI, BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.2,
          height: MediaQuery.of(context).size.width * 0.2,
          child: ElevatedButton(
            onPressed: () {
              if (nroI == 1)
                pickImage();
              else
                pickImage2();
            },
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(
                  color: Color(0xFFFD8641),
                  width: 2,
                ),
              ),
              backgroundColor: Color(0xFFFD8641),
              padding: EdgeInsets.zero,
              minimumSize: Size(44, 44),
            ),
            child: Icon(
              Icons.photo_camera_outlined,
              color: Colors.white,
              size: 50,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        texto1Negrilla(nom, 18),
        SizedBox(
          height: 10,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.2,
          height: MediaQuery.of(context).size.width * 0.2,
          child: nroI == 1
              ? _pickedImage1 == null
                  ? Container(
                      color: Colors.grey[300],
                      child: Icon(
                        Icons.image,
                        color: Colors.white,
                        size: 50,
                      ),
                    )
                  : Container(
                      height: 300,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                          image: FileImage(_pickedImage1!),
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
              : _pickedImage2 == null
                  ? Container(
                      color: Colors.grey[300],
                      child: Icon(
                        Icons.image,
                        color: Colors.white,
                        size: 50,
                      ),
                    )
                  : Container(
                      height: 300,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                          image: FileImage(_pickedImage2!),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
        ),
        nroI == 1
            ? _scanning1
                ? Center(child: CircularProgressIndicator())
                : Icon(
                    Icons.done,
                    size: 40,
                    color: Colors.green,
                  )
            : _scanning2
                ? Center(child: CircularProgressIndicator())
                : Icon(
                    Icons.done,
                    size: 40,
                    color: Colors.green,
                  ),
      ],
    );
  }

  Future<void> pickImage() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.camera);
    if (pickedImage != null) {
      setState(() {
        _pickedImage1 = File(pickedImage.path);
        _scanning1 = true;
      });
      final extractText =
          await FlutterTesseractOcr.extractText(_pickedImage1!.path, language: 'spa',
        args: {
          "psm": "3",
          "preserve_interword_spaces": "1",
        });
      setState(() {
        _extractText1 = extractText ?? '';
        if(_extractText1!=''){
          List<String> palabras = _extractText1.split('No');
          if(palabras.length>2)
          _extractText1 = palabras[1]+"bababa";
        }
        _scanning1 = false;
      });
    }
  }

  Future<void> pickImage2() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.camera);
    if (pickedImage != null) {
      setState(() {
        _pickedImage2 = File(pickedImage.path);
        _scanning2 = true;
      });
      final extractText =
          await FlutterTesseractOcr.extractText(_pickedImage2!.path);
      setState(() {
        _extractText2 = extractText ?? '';
        _scanning2 = false;
      });
    }
  }
}
