import 'package:app_preste/pages/pag4.dart';
import 'package:flutter/material.dart';

class pag3 extends StatefulWidget {
  pag3({Key? key}) : super(key: key);

  @override
  State<pag3> createState() => _pag3State();
}

class _pag3State extends State<pag3> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            // fondo de pantalla
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Color(0xFFF3F6FB),
                shape: BoxShape.rectangle,
              ),
            ),
            // fin fondo de pantalla
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  containerCabecera(context, 0.25),
                  containerCuerpo(context, 0.67),
                  containerBoton(context, 0.08),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
      child: Column(
                          mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.2,
                decoration: BoxDecoration(
                  color: Color(0x00FFFFFF),
                ),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.asset(
                      'assets/images/logo_1_1.png',
                      width: 300,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                  color: Color(0x00FFFFFF),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.asset(
                    'assets/images/imgbarra2.png',
                    width: 300,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Text(
            '¡Felicidades!',
            style: TextStyle(
              fontFamily: 'Patrick Hand',
              color: Colors.white,
              fontSize: 45,
            ),
          )
        ],
      ),
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(35),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'GANASTE',
                      style: TextStyle(
                        fontFamily: 'Patrick Hand',
                        color: Color(0xFFFD8641),
                        fontSize: 30,
                      ),
                    ),
                    Text(
                      'PUNTOS',
                      style: TextStyle(
                        fontFamily: 'Patrick Hand',
                        color: Color(0xFFFD8641),
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.width * 0.3,
                  decoration: BoxDecoration(
                    color: Color(0xFFEF5117),
                    shape: BoxShape.circle,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 5),
                        child: Text(
                          '+10',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Patrick Hand',
                            color: Colors.white,
                            fontSize: 50,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                'assets/imagen4.png',
                width: 300,
                height: 200,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Material(
            color: Colors.transparent,
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(35),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.15,
              decoration: BoxDecoration(
                color: Color(0x2E000000),
                borderRadius: BorderRadius.circular(35),
                border: Border.all(
                  color: Color(0xFF948D88),
                  width: 3,
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Recuerda! con más puntos podrás destacarte y estar más cerca de tu',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Patrick Hand',
                      color: Colors.white,
                      fontSize: 19,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    'objetivo',
                    style: TextStyle(
                      fontFamily: 'Patrick Hand',
                      fontSize: 19,
                      color: Colors.yellow,
                      fontWeight: FontWeight.w600,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
            child: ElevatedButton.icon(
              onPressed: () async {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => pag4()));
              },
              icon: const Icon(
                Icons.arrow_forward,
                size: 15,
              ),
              label: const Text('Adelante'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Color(0xFFF4601F),
                backgroundColor: Colors.white,
                minimumSize: Size(150, 50),
                textStyle: const TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFF4601F),
                  fontSize: 18,
                ),
                side: const BorderSide(
                  color: Color(0xFFF4601F),
                  width: 3,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35),
                ),
                elevation: 2,
                padding: EdgeInsets.zero,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }
}
