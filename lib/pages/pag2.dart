import 'package:app_preste/pages/pag3.dart';
import 'package:app_preste/pages/pagTerminos.dart';
import 'package:flutter/material.dart';

class pag2 extends StatefulWidget {
  pag2({Key? key}) : super(key: key);

  @override
  State<pag2> createState() => _pag2State();
}

class _pag2State extends State<pag2> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            // fondo de pantalla
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Color(0xFFF3F6FB),
                shape: BoxShape.rectangle,
              ),
            ),
            // fin fondo de pantalla
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  containerCabecera(context, 0.13),
                  containerCuerpo(context, 0.79),
                  containerBoton(context, 0.08),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.2,
            height: MediaQuery.of(context).size.height * porcentajeAlto,
            decoration: BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.asset(
                  'assets/images/logo_1_1.png',
                  width: 300,
                  height: 200,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
            height: MediaQuery.of(context).size.height * porcentajeAlto,
            decoration: BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                'assets/images/imgbarra1.png',
                width: 300,
                height: 200,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.fromSTEB(0, 50, 0, 0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.5,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
                          child: ElevatedButton(
                            onPressed: () {
                              print('IconButton pressed...');
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                                side: BorderSide(
                                  color: Color(0xFFFD8641),
                                  width: 2,
                                ),
                              ),
                              backgroundColor: Color(0xFFFD8641),
                              padding: EdgeInsets.zero,
                              minimumSize: Size(44, 44),
                            ),
                            child: Icon(
                              Icons.read_more,
                              color: Colors.white,
                              size: 24,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'Ingresa tu número de celular    ( +10 pts)',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontFamily: 'Open Sans',
                              fontSize: 25,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.13,
                    decoration: BoxDecoration(),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(15, 15, 15, 0),
                            child: TextFormField(
                              obscureText: false,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(
                                  fontFamily: 'Patrick Hand',
                                  fontSize: 18,
                                ),
                                hintText: '77777777',
                                hintStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                  color: Color(0x8D060629),
                                  fontSize: 18,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFF948D88),
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0x00000000),
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0x00000000),
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0x00000000),
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                filled: true,
                                fillColor: Color(0xB8F3F6FB),
                                contentPadding: EdgeInsetsDirectional.fromSTEB(
                                    16, 24, 0, 24),
                              ),
                              textAlign: TextAlign.center,
                              maxLines: null,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 10, 0, 0),
                        child: ElevatedButton.icon(
                          onPressed: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => pag3()));
                          },
                          icon: const Icon(
                            Icons.arrow_forward,
                            size: 15,
                          ),
                          label: const Text('Ingresar'),
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Color(0xFFF4601F),
                            backgroundColor: Colors.white,
                            minimumSize: Size(150, 50),
                            textStyle: const TextStyle(
                              fontFamily: "Poppins",
                              color: Color(0xFFF4601F),
                              fontSize: 18,
                            ),
                            side: const BorderSide(
                              color: Color(0xFFF4601F),
                              width: 3,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(35),
                            ),
                            elevation: 2,
                            padding: EdgeInsets.zero,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
            child: ElevatedButton.icon(
              onPressed: () async {
                Navigator.push(
                      context, MaterialPageRoute(builder: (context) => pagTerminos()));
              },
              icon: const Icon(
                Icons.remove_red_eye,
                size: 20,
              ),
              label: const Text('Terminos y condiciones'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Color(0xFFF4601F),
                backgroundColor: Colors.white,
                minimumSize: Size(150, 50),
                textStyle: const TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFF4601F),
                  fontSize: 18,
                ),
                side: const BorderSide(
                  color: Color(0xFFF4601F),
                  width: 3,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35),
                ),
                elevation: 2,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }
}
