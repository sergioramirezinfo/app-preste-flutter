import 'package:flutter/material.dart';

import '../presentation/styles/color_theme.dart';

class pag6 extends StatefulWidget {
  pag6({Key? key}) : super(key: key);

  @override
  State<pag6> createState() => _pag6State();
}

class _pag6State extends State<pag6> {
  //colores
  Color miColorSelect = Color(0xFFFD8641);
  Color miColor1 = Color(0x007D7777);
  Color miColor2 = Color(0x007D7777);
  List<Color> vColores = [
    Color(0xFFFD8641),
    Color(0x007D7777),
    Color(0x007D7777)
  ];
  List<Color> vColoresLetras = [
    Color.fromARGB(255, 255, 255, 255),
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 0, 0, 0)
  ];
   List<Color> vColoresTexto = [
    Color(0xFFFD8641),
    Color(0x007D7777),
    Color(0x007D7777)
  ];
  List<Color> vColoresLetrasTexto = [
    Color.fromARGB(255, 255, 255, 255),
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 0, 0, 0)
  ];

  // imagenes
  List<String> vectorImagenes = [
    'default',
    'assets/images/soltero.png',
    'assets/images/casado.png'
  ];
  List<String> nombreImagen = [
    'default',
    'Soltero(a)',
    'Casado(a)'
  ];
  List<String> nombreEstado = [
    'default',
    'Dependiente',
    'Independiente'
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            // fondo de pantalla
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Color(0xFFF3F6FB),
                shape: BoxShape.rectangle,
              ),
            ),
            // fin fondo de pantalla
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  containerCabecera(context, 0.13),
                  containerCuerpo(context, 0.74),
                  containerBoton(context, 0.12),
                  containerRelleno(context, 0.02, 1)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  cambioColor(BuildContext context, int nro) {
    setState(() {
      vColores = [
        Color(0xFFFD8641),
        Color(0x007D7777),
        Color(0x007D7777)
      ];
      vColores[nro] = vColores[0];

      vColoresLetras = [
        Color.fromARGB(255, 255, 255, 255),
        Color.fromARGB(255, 0, 0, 0),
        Color.fromARGB(255, 0, 0, 0)
      ];
      vColoresLetras[nro] = vColoresLetras[0];
    });
  }
  cambioColorTexto(BuildContext context, int nro) {
    setState(() {
      vColoresTexto = [
        Color(0xFFFD8641),
        Color(0x007D7777),
        Color(0x007D7777)
      ];
      vColoresTexto[nro] = vColoresTexto[0];

      vColoresLetrasTexto = [
        Color.fromARGB(255, 255, 255, 255),
        Color.fromARGB(255, 0, 0, 0),
        Color.fromARGB(255, 0, 0, 0)
      ];
      vColoresLetrasTexto[nro] = vColoresLetrasTexto[0];
    });
  }

  Widget contenedorGridView(BuildContext context, int nroI) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      height: 140,
      decoration: BoxDecoration(
        color: vColores[nroI],
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Color(0xFFCBCBCB),
          width: 5,
        ),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.fromSTEB(12, 12, 12, 12),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 8,
              child: GestureDetector(
                onTap: () {
                  cambioColor(context, nroI);
                },
                child: AspectRatio(
                  aspectRatio:
                      1, // ajusta la relación de aspecto según tus necesidades
                  child: Image.asset(vectorImagenes[nroI],
                      fit: BoxFit
                          .contain // ajusta la propiedad fit según tus necesidades
                      ),
                ),
              ),
            ),
            Expanded(
                flex: 2, child: texto2Normal(nombreImagen[nroI], 16, nroI)),
          ],
        ),
      ),
    );
  }
Widget contenedorGridViewTexto(BuildContext context, int nroI) {
    
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      decoration: BoxDecoration(
        color: vColoresTexto[nroI],
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Color(0xFFCBCBCB),
          width: 5,
        ),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.fromSTEB(12, 12, 12, 12),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                 child: GestureDetector(
                   onTap: () {
                  cambioColorTexto(context, nroI);
                },
                  child: texto3Normal(nombreEstado[nroI], 16, nroI),
                  )),
          ],
        ),
      ),
    );
  }
  

  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.2,
            height: MediaQuery.of(context).size.height * porcentajeAlto,
            decoration: BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.asset(
                  'assets/images/logo_1_1.png',
                  width: 300,
                  height: 200,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
            height: MediaQuery.of(context).size.height * porcentajeAlto,
            decoration: BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                'assets/images/imgbarra3.png',
                width: 300,
                height: 200,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          containerRelleno(context, 0.02, 1),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  flex: 1,
                  child: texto1Negrilla('Ingresos Mensuales:', 26),
                ),
                texto1Puntaje('(+ 10 pts)', 18),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(24, 0, 24, 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
                    child: Container(
                      width: 180,
                      child: TextFormField(
                        autofocus: false,
                        obscureText: false,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xFFBBAACC),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0x00000000),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0x00000000),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0x00000000),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               Flexible(
                  flex: 1,
                  child: texto1Negrilla('Como generas ingresos:', 26),
                ),
                 Padding(
                          padding: EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
                          child: ElevatedButton(
                            onPressed: () {
                              print('IconButton pressed...');
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                                side: BorderSide(
                                  color: Color(0xFFFD8641),
                                  width: 2,
                                ),
                              ),
                              backgroundColor: Color(0xFFFD8641),
                              padding: EdgeInsets.zero,
                              minimumSize: Size(44, 44),
                            ),
                            child: Icon(
                              Icons.question_mark_rounded,
                              color: Colors.white,
                              size: 24,
                            ),
                          ),
                        ),
                
                texto1Puntaje('(+ 10 pts)', 18),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.95,
            
            decoration: const BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
              //qr
              child: GridView(
                
                padding: EdgeInsets.zero,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 15,
                  childAspectRatio: 3,
                  mainAxisSpacing: 10,
                ),
                
                primary: false,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                
                children: [
                  contenedorGridViewTexto(context, 1),
                  contenedorGridViewTexto(context, 2)
                ],
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  flex: 1,
                  child: texto1Negrilla('Estado civil:', 26),
                ),
                texto1Puntaje('(+ 10 pts)', 18),
              ],
            ),
          ),
          
          Container(
            width: MediaQuery.of(context).size.width * 0.95,
            decoration: const BoxDecoration(
              color: Color(0x00FFFFFF),
            ),
            child: Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
              //qr
              child: GridView(
                padding: EdgeInsets.zero,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 15,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1,
                ),
                primary: false,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                children: [
                  contenedorGridView(context, 1),
                  contenedorGridView(context, 2)
                ],
              ),
            ),
          ),
        containerRelleno(context, 0.02, 1),
        ],
      ),
    );
  }

  Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Center(
        child: Container(
          width: 190,
          decoration: BoxDecoration(
            border: Border.all(color: ColorThemeFont.colorThemeOrange,width: 3),
            borderRadius: BorderRadius.circular(35),
            color: Colors.white
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(35),
            child: TextButton(
               onPressed: () async {
                Navigator.pushNamed(
                  context,
                  '/pagina7',
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 20),
                child: Row(
                  children: [
                    Icon(Icons.arrow_forward_outlined,color: ColorThemeFont.colorThemeOrange,),
                    const SizedBox(width: 20,),
                    Text(
                      'Adelante',
                      style: TextStyle(
                        fontSize: 25,
                        color: ColorThemeFont.colorThemeOrange,
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    )
        ],
      ),
    );
  }

  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }

  Widget texto1Normal(String x, double tam) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: 'Poppins',
        fontSize: tam,
        letterSpacing: 1,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget texto2Normal(String x, double tam, int indice) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: 'Poppins',
        color: vColoresLetras[indice],
        fontSize: tam,
        letterSpacing: 1,
        fontWeight: FontWeight.w600,
      ),
    );
  }
   Widget texto3Normal(String x, double tam, int indice) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: 'Poppins',
        color: vColoresLetrasTexto[indice],
        fontSize: tam,
        letterSpacing: 1,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget texto1Negrilla(String x, double tam) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: 'Poppins',
        fontSize: tam,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget texto1Puntaje(String x, double tam) {
    return Text(
      x,
      style: TextStyle(
        fontFamily: x,
        color: Color(0xFFE54B0A),
        fontSize: tam,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
