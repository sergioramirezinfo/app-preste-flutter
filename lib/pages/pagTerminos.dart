import 'package:app_preste/pages/pag2.dart';
import 'package:flutter/material.dart';

class pagTerminos extends StatefulWidget {
  pagTerminos({Key? key}) : super(key: key);

  @override
  State<pagTerminos> createState() => _pagTerminosState();
}

class _pagTerminosState extends State<pagTerminos> {
  // imagenes
  List<String> vectorImagenes = [
    'default',
    'assets/images/casa.png',
    'assets/images/auto.png',
    'assets/images/avion.png',
    'assets/images/negocios.png',
    'assets/images/salud.png',
    'assets/images/Otro.png'
  ];
  List<String> nombreImagen = [
    'default',
    'casa',
    'auto',
    'avion',
    'negocios',
    'salud',
    'Otro'
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Color.fromARGB(0, 0, 0, 0),
        body: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 1,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 255, 255, 255),
                Color(0xFFFDEAB1),
                Color(0xFFFD8641),
              ],
              stops: [0, 0.6, 1],
              begin: AlignmentDirectional(0, 1),
              end: AlignmentDirectional(0, -1),
            ),
          ),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(0, 1),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(10, 100, 10, 0),
                      child: Material(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height * 0.65,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 4,
                                color: Color.fromARGB(255, 255, 255, 255),
                                offset: Offset(1, -1),
                              )
                            ],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Padding(
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      0, 16, 0, 0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Padding(
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            0, 0, 16, 0),
                                        child: ElevatedButton(
                                          onPressed: () {
                                            print('IconButton pressed...');
                                          },
                                          style: ElevatedButton.styleFrom(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                              side: BorderSide(
                                                color: Color(0xFFFD8641),
                                                width: 2,
                                              ),
                                            ),
                                            backgroundColor: Color(0xFFFD8641),
                                            padding: EdgeInsets.zero,
                                            minimumSize: Size(44, 44),
                                          ),
                                          child: Icon(
                                            Icons.remove_red_eye,
                                            color: Colors.white,
                                            size: 24,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: texto1Normal(
                                          'Terminos y condiciones',18
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      0, 15, 0, 10),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        texto1Normal(
                                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                                            17),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15,),
                    containerBoton(context, 0.1)
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 124,
                decoration: BoxDecoration(
                  color: Color(0x00FFFFFF),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  color: Color(0x00FD8641),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.height * 0.15,
                      decoration: BoxDecoration(
                        color: Color(0x00FFFFFF),
                      ),
                      child: Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.asset(
                            'assets/images/logo_1_1.png',
                            width: 300,
                            height: 200,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }


  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }

  Widget texto1Normal(String x, double tam) {
    return Text(
      x,
      textAlign: TextAlign.justify,
      style: TextStyle(
        fontFamily: 'Poppins',
        fontSize: tam,
        letterSpacing: 1,
        fontWeight: FontWeight.w500,
      ),
    );
  }
   Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
            child: ElevatedButton.icon(
              onPressed: () async {
                 Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => pag2()));
              },
              icon: const Icon(
                Icons.arrow_forward,
                size: 15,
              ),
              label: const Text('Volver'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Color(0xFFF4601F),
                backgroundColor: Colors.white,
                minimumSize: Size(150, 50),
                textStyle: const TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFF4601F),
                  fontSize: 18,
                ),
                side: const BorderSide(
                  color: Color(0xFFF4601F),
                  width: 3,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35),
                ),
                elevation: 2,
                padding: EdgeInsets.zero,
              ),
            ),
          ),
        ],
      ),
    );
  }

}
