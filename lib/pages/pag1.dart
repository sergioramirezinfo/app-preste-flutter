import 'package:flutter/material.dart';

class pag1 extends StatefulWidget {
  pag1({Key? key}) : super(key: key);

  @override
  State<pag1> createState() => _pag1State();
}

class _pag1State extends State<pag1> {

  // imagenes
  List<String> vectorImagenes = [
    'default',
    'assets/images/casa.png',
    'assets/images/auto.png',
    'assets/images/avion.png',
    'assets/images/negocios.png',
    'assets/images/salud.png',
    'assets/images/Otro.png'
  ];
  List<String> nombreImagen = [
    'default',
    'casa',
    'auto',
    'avion',
    'negocios',
    'salud',
    'Otro'
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            // fondo de pantalla
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Color(0xFFF3F6FB),
                shape: BoxShape.rectangle,
              ),
            ),
            // fin fondo de pantalla
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  containerCabecera(context, 0.13),
                  containerCuerpo(context, 0.79),
                  containerBoton(context, 0.08),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget containerCabecera(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
        ),
      ),
      
    );
  }

  Widget containerCuerpo(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
      
    );
  }

  Widget containerBoton(BuildContext context, double porcentajeAlto) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0x00FFFFFF),
      ),
      
    );
  }

  Widget containerRelleno(
      BuildContext context, double porcentajeAlto, double porcentajeAncho) {
    return Container(
      width: MediaQuery.of(context).size.width * porcentajeAncho,
      height: MediaQuery.of(context).size.height * porcentajeAlto,
      decoration: BoxDecoration(
        color: Color(0xFFF3F6FB),
      ),
    );
  }
}
