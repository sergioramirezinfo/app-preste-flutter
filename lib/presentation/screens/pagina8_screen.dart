import 'package:flutter/material.dart';
import 'package:app_preste/presentation/widgets/commons/app_bar_widget.dart';
import 'package:app_preste/presentation/widgets/commons/text_button_decoration.dart';

class Pagina8Screen extends StatelessWidget {
  const Pagina8Screen({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFFFB8641),
            Color(0xFFFBEAB1),
            Colors.white,
          ]
        )
      ),
      child: ListView(
        // padding: const EdgeInsets.only(top: 10),
        children: [
          
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: AppBarWidget(size: size),
          ),

          SizedBox(height: size.height * 0.07,),
          
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.6,
              decoration: BoxDecoration(
                color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      blurRadius: 10,
                      color: Colors.black54,
                      offset: Offset(2, -2),
                    )
                  ],
                  borderRadius: BorderRadius.circular(20),
              ),
              child: const Column(
                children: [
                  CardBody1Screen8(),
                  CardBody2Screen8(),
                  CardBody3Screen8(),
                  CardBody4Screen8(),
                  CardBody5Screen8(),
                ],
              ),
            ),
          ),
          SizedBox(
            height: size.height * 0.15,
            child: const TextButtonDecoration(ruta: "/pagina9",),
          )
        ],
      ),
    );
  }
}




class CardBody1Screen8 extends StatelessWidget{
  const CardBody1Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(20, 16, 20, 0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: const Color(0xFFFD8641),
                borderRadius: BorderRadius.circular(100),
                border: Border.all(
                  width: 2,
                  color: const Color(0xFFFD8641)
                )
              ),
              child: const Icon(
                color: Colors.white,
                Icons.linked_camera,
                size: 24,
              ),
            ),
          ),
          Expanded(
            child: Text(
              'Toma una fotografia a tu carnet de identidad    (+100 pts)',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 20,color: Colors.black),
            ),
          ),
        ],
      ),
    );

  }
}

class CardBody2Screen8 extends StatelessWidget{
  const CardBody2Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(0, 30, 0, 0),
          child: GestureDetector(
            onTap: (){},
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: MediaQuery.of(context).size.width * 0.4,
                  decoration: const BoxDecoration(
                    color: Color(0x33131D2F),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.photo_camera_outlined,
                    color: Colors.black,
                    size: 90,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(0, 30, 0, 0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              GestureDetector(
                onTap: (){},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: MediaQuery.of(context).size.width * 0.4,
                  decoration: const BoxDecoration(
                    color: Color(0x33131D2F),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.photo_camera_outlined,
                    color: Colors.black,
                    size: 90,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
  );
  }
}

class CardBody3Screen8 extends StatelessWidget{
  const CardBody3Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(0, 15, 0, 0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'ANVERSO',
            style: Theme.of(context).textTheme.bodyMedium?.copyWith(
              fontSize: 25,
              fontWeight: FontWeight.w500
            ),
          ),
          Text(
            'REVERSO',
            style: Theme.of(context).textTheme.bodyMedium?.copyWith(
              fontSize: 25,
              fontWeight: FontWeight.w500
            ),
          ),
        ],
      ),
    );
  }
}

class CardBody4Screen8 extends StatelessWidget{
  const CardBody4Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(20, 16, 20, 0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: const Color(0xFFFD8641),
                borderRadius: BorderRadius.circular(100),
                border: Border.all(
                  width: 2,
                  color: const Color(0xFFFD8641)
                )
              ),
              child: const Icon(
                color: Colors.white,
                Icons.read_more,
                size: 24,
              ),
            ),
          ),
          Expanded(
            child: Text(
              'Ingresa tu número de carnet de identidad:         (+50 pts)',
              // style: FlutterFlowTheme.of(context).headlineMedium.override(
              //       fontFamily: 'Patrick Hand',
              //       fontSize: 25,
              // ),
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontSize: 20,
                color: Colors.black
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardBody5Screen8 extends StatelessWidget{
  const CardBody5Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Material(
      child: Expanded(
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(20, 15, 20, 0),
          child: TextFormField(
            obscureText: false,
            decoration: InputDecoration(
              
              labelStyle: Theme.of(context).textTheme.titleSmall?.copyWith(
                fontSize: 18
              ),
              hintText: '4568525',
              hintStyle: Theme.of(context).textTheme.titleSmall?.copyWith(
                fontSize: 18,
                color: const Color(0x8D060629)
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  // color: FlutterFlowTheme.of(context).accent3,
                  width: 3,
                  color: Color(0x8D060629)
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0x00000000),
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0x00000000),
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0x00000000),
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              filled: true,
              // fillColor: FlutterFlowTheme.of(context).secondaryBackground,
              contentPadding: const EdgeInsetsDirectional.fromSTEB(16, 24, 0, 24),
            ),
            style: Theme.of(context).textTheme.titleMedium,
            // maxLines: null,
            // validator: _model.phoneNumberControllerValidator.asValidator(context),
          ),
        ),
    ),
    );

  }
}