import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class Pagina12Screen extends StatelessWidget {
  const Pagina12Screen({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 25,bottom: 5),
        child: ListView(
          children: [
            Image(
              image: const AssetImage('assets/pagina5.jpg'),
              height: size.height * 0.20,
            ),
            Center(
              child: Text(
                '!Felicidades!',
                style: GoogleFonts.pacifico(
                  fontSize: 54,
                  color: const Color(0xFFED5501),
                  fontWeight: FontWeight.bold
                )
              )
            ),
            Image(
              image: const AssetImage('assets/pagina6.png'),
              height: size.height * 0.30,
              fit: BoxFit.fitHeight,
            ),
            const SizedBox(height: 10,),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'Muy pronto tendremos novedades para ayudarte en tu objetivo',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.pacifico(
                    fontSize: 35,fontWeight: FontWeight.w500,height: 1.5
                  ),
                ),
              )
            ),
            const SizedBox(height: 10,),
            Center(
              child: IconButton(
                icon: const Icon(
                  Icons.home,
                  color: Color(0xFFED5501),
                  size: 30,
                ),
                onPressed: (){},
              ),
            )
          ],
        ),
      ),
    );
  }
}