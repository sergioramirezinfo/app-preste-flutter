import 'package:flutter/material.dart';
import 'package:app_preste/presentation/widgets/commons/app_bar_widget.dart';
import 'package:app_preste/presentation/widgets/commons/text_button_decoration.dart';

class Pagina11Screen extends StatelessWidget {
  const Pagina11Screen({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFFFB8641),
            Color(0xFFFBEAB1),
            Colors.white,
          ]
        )
      ),
      child: ListView(
        // padding: const EdgeInsets.only(top: 10),
        children: [
          
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: AppBarWidget(size: size),
          ),

          SizedBox(height: size.height * 0.07,),
          
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.6,
              decoration: BoxDecoration(
                color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      blurRadius: 10,
                      color: Colors.black54,
                      offset: Offset(2, -2),
                    )
                  ],
                  borderRadius: BorderRadius.circular(20),
              ),
              child: const Column(
                children: [
                  CardBody1Screen8(),
                  CardBody2Screen8(),
                  CardBody4Screen8(),
                  CardBody3Screen8(text: 'Subir Archivo',),
                ],
              ),
            ),
          ),
          SizedBox(
            height: size.height * 0.15,
            child: const TextButtonDecoration(ruta: "/pagina12",),
          )
        ],
      ),
    );
  }
}


class CardBody1Screen8 extends StatelessWidget{
  const CardBody1Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(20, 16, 20, 0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: const Color(0xFFFD8641),
                borderRadius: BorderRadius.circular(100),
                border: Border.all(
                  width: 2,
                  color: const Color(0xFFFD8641)
                )
              ),
              child: const Icon(
                color: Colors.white,
                Icons.linked_camera,
                size: 24,
              ),
            ),
          ),
          Expanded(
            child: Text(
              'Toma una fotografia o suibe tu plantilla de Pago en formato pdf: ( +200 pts )',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 20,height: 1.5),
            ),
          ),
        ],
      ),
    );

  }
}

class CardBody2Screen8 extends StatelessWidget{
  const CardBody2Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
          child: GestureDetector(
            onTap: (){},
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  height: MediaQuery.of(context).size.width * 0.5,
                  decoration: const BoxDecoration(
                    color: Color(0xFFFD8641),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.photo_camera_outlined,
                    color: Colors.white,
                    size: 90,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
  );
  }
}

class CardBody3Screen8 extends StatelessWidget{
  final String text;
  const CardBody3Screen8({
    super.key, 
    required this.text
  });

  @override
  Widget build(BuildContext context){
    return Text(
      text,
      style: Theme.of(context).textTheme.bodyMedium?.copyWith(
        fontSize: 15,
        fontWeight: FontWeight.w500
      ),
    );
  }
}

class CardBody4Screen8 extends StatelessWidget{
  const CardBody4Screen8({super.key});

  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(0, 30, 0, 0),
          child: GestureDetector(
            onTap: (){},
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.15,
                  height: MediaQuery.of(context).size.width * 0.14,
                  decoration: BoxDecoration(
                    color: const Color(0xFFFD8641),
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: const Icon(
                    Icons.file_upload,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
  );
  }
}

