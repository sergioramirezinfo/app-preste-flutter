import 'package:flutter/material.dart';
import 'package:app_preste/presentation/widgets/commons/app_bar_widget.dart';
import 'package:app_preste/presentation/widgets/commons/body_image_1.dart';
import 'package:app_preste/presentation/widgets/commons/pagina7_widget_1.dart';
import 'package:app_preste/presentation/widgets/commons/text_button_decoration.dart';

class Pagina9Screen extends StatelessWidget {
  const Pagina9Screen({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    const textStyle = TextStyle(
      // fontSize: size.width * 0.05,
      fontSize: 20,
      color: Colors.white,
      fontWeight: FontWeight.bold,
      height: 1.4,
    );
    const textStyle1 = TextStyle(
      // fontSize: size.width * 0.05,
      fontSize: 20,
      color: Colors.yellow,
      fontWeight: FontWeight.bold,
      height: 1.4,
      decoration: TextDecoration.underline
    );
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        // physics: BouncingScrollPhysics(),
        children: [
          BackgroundAppBar(size: size),
          const Pagina7Widget1(),
          BodyImage1(size: size,height: size.height * 0.25,imageUrl: 'assets/imagen4.png'),
          const _CardText1(textStyle: textStyle, textStyle1: textStyle1),
          const TextButtonDecoration(ruta: '/pagina10',)
        ],
      ),
    );
  }
}




class _CardText1 extends StatelessWidget {
  const _CardText1({
    required this.textStyle,
    required this.textStyle1,
  });

  final TextStyle textStyle;
  final TextStyle textStyle1;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: Colors.grey[600]!,width: 2)
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text('Recuerda! con mas puntos podras',style: textStyle,),
            Text('destacarte y estar mas cerca de tu',style: textStyle),
            Text('Objetivo',style: textStyle1),
          ],
        ),
      ),
    );
  }
}

