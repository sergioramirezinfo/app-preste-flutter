import 'package:flutter/material.dart';
import 'package:app_preste/presentation/styles/color_theme.dart';

class Pagina7Widget1 extends StatelessWidget {
  const Pagina7Widget1({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SizedBox(
      height: size.height * 0.20,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Ganaste',style: TextStyle(fontSize: size.height * 0.06,color: ColorThemeFont.colorThemeOrange,height: 1.0),),
                Text('Puntos',style: TextStyle(fontSize: size.height * 0.06,fontWeight: FontWeight.bold,color: ColorThemeFont.colorThemeOrange)),
              ],
            ),
            
            Container(
              width: MediaQuery.of(context).size.width * 0.35,
              height: MediaQuery.of(context).size.width * 0.35,
              decoration: BoxDecoration(
                color: ColorThemeFont.colorThemeOrange,
                borderRadius: BorderRadius.circular(200),
                border: Border.all(color: Colors.yellow,width: 2)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 12,),
                  Transform.translate(
                    offset: const Offset(10, -15),
                    child: Text(
                      '+30',
                      style: TextStyle(
                        fontSize: size.height * 0.08,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}