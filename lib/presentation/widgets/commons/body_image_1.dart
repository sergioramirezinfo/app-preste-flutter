import 'package:flutter/material.dart';


class BodyImage1 extends StatelessWidget {
  final double height;
  final String imageUrl;

  const BodyImage1({
    super.key,
    required this.size, 
    required this.height,
    required this.imageUrl
  });

  final Size size;
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20,right: 20,bottom: 10),
      child: Image(
        height: height,
        image: AssetImage(imageUrl),
      ),
    );
  }
}