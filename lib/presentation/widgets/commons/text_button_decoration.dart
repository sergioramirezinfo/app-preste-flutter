import 'package:flutter/material.dart';
import 'package:app_preste/presentation/styles/color_theme.dart';

class TextButtonDecoration extends StatelessWidget {
  final String ruta;
  const TextButtonDecoration({
    super.key,
    required this.ruta
  });

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Center(
        child: Container(
          width: 190,
          decoration: BoxDecoration(
            border: Border.all(color: ColorThemeFont.colorThemeOrange,width: 3),
            borderRadius: BorderRadius.circular(35),
            color: Colors.white
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(35),
            child: TextButton(
              onPressed: (){
                Navigator.pushReplacementNamed(context, ruta);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 20),
                child: Row(
                  children: [
                    Icon(Icons.arrow_forward_outlined,color: ColorThemeFont.colorThemeOrange,),
                    const SizedBox(width: 20,),
                    Text(
                      'Adelante',
                      style: TextStyle(
                        fontSize: 25,
                        color: ColorThemeFont.colorThemeOrange,
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}