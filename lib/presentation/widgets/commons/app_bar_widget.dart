import 'package:flutter/material.dart';


class BackgroundAppBar extends StatelessWidget {
  final Size size;
  const BackgroundAppBar({
    super.key,
    required this.size
  });

  @override
  Widget build(BuildContext context) {

    return Container(
      width: double.infinity,
      height: size.height * 0.28,
      padding: const EdgeInsets.only(right: 10,left: 10),
      decoration: const BoxDecoration(
        color: Color(0xFFFD8641),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
        )
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            AppBarWidget(size: size,),
            Text('!Felicidades!',style: TextStyle(fontSize: size.height * 0.06,color: Colors.white),),
          ],
        ),
      ),
    );
  }
}


class AppBarWidget extends StatelessWidget {
  final Size size;
  const AppBarWidget({
    super.key, 
    required this.size, 
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Image(
            image: const AssetImage('assets/icono1.jpg'),
            height: size.height * 0.08,
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Image(
            image: const AssetImage('assets/images/imgbarra4.png'),
            height: size.height * 0.08,
          ),
        ),
      ],
    );
  }
}